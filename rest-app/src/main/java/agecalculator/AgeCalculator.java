package agecalculator;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class AgeCalculator {
	private LocalDate birthday;
	private Age age;
	
	/**
	 * @param birthday
	 */
	public AgeCalculator(LocalDate birthday) {
		super();
		this.birthday = birthday;
	}

	/**
	 * @return the birthday
	 */
	public LocalDate getBirthday() {
		return birthday;
	}

	/**
	 * @param birthday the birthday to set
	 */
	public void setBirthday(LocalDate birthday) {
		this.birthday = birthday;
	}

	/**
	 * @return the age
	 */
	public Age getAge() {
		return age;
	}

	/**
	 * @param age the age to set
	 */
	public void setAge(Age age) {
		this.age = age;
	}
	
	public Age calculateAge(LocalDate birthday, LocalDate currentDate) {
		Age age = new Age();
		
		LocalDate tmpDate = LocalDate.from(birthday);
		
		age.setYears((int)tmpDate.until(currentDate, ChronoUnit.YEARS));
		tmpDate = tmpDate.plusYears(age.getYears());
		
		age.setMonths((int)tmpDate.until(currentDate, ChronoUnit.MONTHS));
		tmpDate = tmpDate.plusMonths(age.getMonths());
		
		age.setDays((int)tmpDate.until(currentDate, ChronoUnit.DAYS));
		tmpDate = tmpDate.plusDays(age.getDays());
		
		System.out.println(age.toString());
		
		return age;
	}
	
	public Age calculateAge(LocalDate currentDate) {
		return calculateAge(this.birthday, currentDate);
	}
}
