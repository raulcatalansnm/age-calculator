package com.rcatalan.backend.repository;

import com.rcatalan.backend.domain.Persona;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the Persona entity.
 */
@SuppressWarnings("unused")
public interface PersonaRepository extends MongoRepository<Persona,String> {

}
