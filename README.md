# Calculadora de edad y Backend #

## Descripción ##
El software construido, es una aplicación Web que permite obtener y almacenar la edad de una persona. La solución se compone de un servicio REST encargado de calcular y una aplicación monolítica que se integra de una interfaz gráfica de tipo Web y servicios para almacenar y recuperar los datos de las personas en un motor de datos mongoDB.

## Servicio REST ##
El servicio para el cálculo de la edad fue construido con Spark Framework, el cual permite la creación de microservicios en Java 8. Fue desplegado en el servidor en formato jar auto-ejecutable e integrado como demonio del sistema operativo para iniciar y finalizar su ejecución en nivel 3 de arranque:

```
#!bash

/etc/init.d/age-calculator
```


La petición es de tipo POST, utiliza estructuras de datos JSON tanto para la solicitud como para la respuesta. Sí los parámetros de entrada son incorrectos la respuesta del servicio será un HTTP 500. A continuación, se detallan los modelos de solicitud y respuesta:


### Modelo de la solicitud ###
```                                          
#!java
{
    birthday (string, "yyyy-MM-dd"),
    today (string, "yyyy-MM-dd")
}

```

Ejemplo:
```
#!json

{
    "birthday" : "1980-02-08",
    "today" : "2017-02-27"
}
```

### Modelo de la respuesta ###
```
#!java
{
	years (integer),
	months(integer),
	days (integer)
}
```

Ejemplo:
```
#!json
{
    "years" : 37,
    "months" : 0,
    "days" : 19
}
```

## Backend ##
Para consumir el servicio REST se definió la construcción de una aplicación Web para actuar de Backend. Para su desarrollo se utilizó el stack JHipster ya que permite un desarrollo rápido y una infraestructura robusta (con Java en el servidor). La aplicación se presenta vía HTTP en el puerto 80, por tanto, se inicia en la url http://ip_servidor. En la página de inicio se detalla la forma de acceso y para ello se pueden utilizar las credenciales (user / user) o (admin / admin), las que cuentan con restricciones de usuario básico y de administrador. Luego de un login correcto, en la barra superior se habilita la opción Persona, por medio de la cual es posible interactuar con el servicio REST, recuperar y almacenar la información consultada.

![Captura de pantalla 2017-02-27 a las 22.36.39.png](https://bitbucket.org/repo/yx6k7E/images/4076679419-Captura%20de%20pantalla%202017-02-27%20a%20las%2022.36.39.png)

![Captura de pantalla 2017-02-27 a las 22.38.30.png](https://bitbucket.org/repo/yx6k7E/images/831351983-Captura%20de%20pantalla%202017-02-27%20a%20las%2022.38.30.png)

## Código Fuente ##
El código fuente fue versionado con GIT y utilizando el repositorio de Bitbucket, el cual está disponible en la URL https://bitbucket.org/raulcatalansnm/age-calculator.
Se construyeron dos proyectos de tipo Maven, uno para Backend y el otro para servicio REST.

## Swagger ##
Dentro del Backend y al ingresar con un perfil de administrador se puede acceder a la interfaz de Swagger para revisar los servicios utilizados por el propio Backend. Esta documentación se encuentra disponible en la ruta: Administración -> API.

![Captura de pantalla 2017-02-27 a las 22.25.53.png](https://bitbucket.org/repo/yx6k7E/images/2089892302-Captura%20de%20pantalla%202017-02-27%20a%20las%2022.25.53.png)


## Servidor Web ##
El servidor Web utilizado fue nginx. Se configuró como proxy inverso hacia el puerto TCP 80 utilizando el siguiente esquema:

```
#!bash

REST (calculador de edad)	*:9090/	->	*:80/age
Backend (aplicación Web)	*:8080/	->	*:80/

```

## Docker ##
Se implementaron dos contenedores docker, uno para el servicio de MongoDB y otro para la aplicación Web. El inicio y cierre de ambos contenedores se configuraron como demonio del sistema operativo en nivel 3:

```
#!bash

/etc/init.d/backend-app
```


## Respaldos ##
Para realizar los respaldos de la base de datos se definieron dos tareas en el crontab del usuario root.
1. Respaldo diario a las 3 AM:
```
#!bash

* 3 * * * docker container exec -d backend-mongodb mongodump --db backend --out /mongodbdump/dumps_daily/bd_$(date +"\%Y-\%m-\%d").dump --quiet && chown proftpd:nogroup -R /respaldos && chmod g+w -R /respaldos
```

2. Empaquetado semanal (3 horas después del respaldo diario):
```
#!bash

0 6 * * 0 tar cf /respaldos/dumps_weekly/bd_$(date +"\%Y-\%m-\%d").tar /respaldos/dumps_daily/ && chown proftpd:nogroup -R /respaldos && chmod g+r -R /respaldos
```



## FTP ##
Para el acceso a los respaldos, se implementó un servidor FTP con los usuarios (writeuser / writeuser) con permisos de escritura y (readuser / readuser) con permisos de sólo lectura, ambos con acceso al directorio /respaldos/.