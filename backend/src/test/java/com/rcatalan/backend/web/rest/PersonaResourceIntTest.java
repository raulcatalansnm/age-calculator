package com.rcatalan.backend.web.rest;

import com.rcatalan.backend.BackendApp;

import com.rcatalan.backend.domain.Persona;
import com.rcatalan.backend.repository.PersonaRepository;
import com.rcatalan.backend.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PersonaResource REST controller.
 *
 * @see PersonaResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BackendApp.class)
public class PersonaResourceIntTest {

    private static final String DEFAULT_RUN = "4-K";
    private static final String UPDATED_RUN = "74-K";

    private static final String DEFAULT_NOMBRE_COMPLETO = "AAAAAAAAAA";
    private static final String UPDATED_NOMBRE_COMPLETO = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_FECHA_NACIMIENTO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_FECHA_NACIMIENTO = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_SEXO = "F";
    private static final String UPDATED_SEXO = "M";

    private static final Integer DEFAULT_EDAD_AGNOS = 1;
    private static final Integer UPDATED_EDAD_AGNOS = 2;

    private static final Integer DEFAULT_EDAD_MESES = 1;
    private static final Integer UPDATED_EDAD_MESES = 2;

    private static final Integer DEFAULT_EDAD_DIAS = 1;
    private static final Integer UPDATED_EDAD_DIAS = 2;

    private static final LocalDate DEFAULT_FECHA_CONSULTA = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_FECHA_CONSULTA = LocalDate.now(ZoneId.systemDefault());

    @Autowired
    private PersonaRepository personaRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    private MockMvc restPersonaMockMvc;

    private Persona persona;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
            PersonaResource personaResource = new PersonaResource(personaRepository);
        this.restPersonaMockMvc = MockMvcBuilders.standaloneSetup(personaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Persona createEntity() {
        Persona persona = new Persona()
                .run(DEFAULT_RUN)
                .nombreCompleto(DEFAULT_NOMBRE_COMPLETO)
                .fechaNacimiento(DEFAULT_FECHA_NACIMIENTO)
                .sexo(DEFAULT_SEXO)
                .edadAgnos(DEFAULT_EDAD_AGNOS)
                .edadMeses(DEFAULT_EDAD_MESES)
                .edadDias(DEFAULT_EDAD_DIAS)
                .fechaConsulta(DEFAULT_FECHA_CONSULTA);
        return persona;
    }

    @Before
    public void initTest() {
        personaRepository.deleteAll();
        persona = createEntity();
    }

    @Test
    public void createPersona() throws Exception {
        int databaseSizeBeforeCreate = personaRepository.findAll().size();

        // Create the Persona

        restPersonaMockMvc.perform(post("/api/personas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(persona)))
            .andExpect(status().isCreated());

        // Validate the Persona in the database
        List<Persona> personaList = personaRepository.findAll();
        assertThat(personaList).hasSize(databaseSizeBeforeCreate + 1);
        Persona testPersona = personaList.get(personaList.size() - 1);
        assertThat(testPersona.getRun()).isEqualTo(DEFAULT_RUN);
        assertThat(testPersona.getNombreCompleto()).isEqualTo(DEFAULT_NOMBRE_COMPLETO);
        assertThat(testPersona.getFechaNacimiento()).isEqualTo(DEFAULT_FECHA_NACIMIENTO);
        assertThat(testPersona.getSexo()).isEqualTo(DEFAULT_SEXO);
        assertThat(testPersona.getEdadAgnos()).isEqualTo(DEFAULT_EDAD_AGNOS);
        assertThat(testPersona.getEdadMeses()).isEqualTo(DEFAULT_EDAD_MESES);
        assertThat(testPersona.getEdadDias()).isEqualTo(DEFAULT_EDAD_DIAS);
        assertThat(testPersona.getFechaConsulta()).isEqualTo(DEFAULT_FECHA_CONSULTA);
    }

    @Test
    public void createPersonaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = personaRepository.findAll().size();

        // Create the Persona with an existing ID
        Persona existingPersona = new Persona();
        existingPersona.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restPersonaMockMvc.perform(post("/api/personas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(existingPersona)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<Persona> personaList = personaRepository.findAll();
        assertThat(personaList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    public void checkRunIsRequired() throws Exception {
        int databaseSizeBeforeTest = personaRepository.findAll().size();
        // set the field null
        persona.setRun(null);

        // Create the Persona, which fails.

        restPersonaMockMvc.perform(post("/api/personas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(persona)))
            .andExpect(status().isBadRequest());

        List<Persona> personaList = personaRepository.findAll();
        assertThat(personaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void getAllPersonas() throws Exception {
        // Initialize the database
        personaRepository.save(persona);

        // Get all the personaList
        restPersonaMockMvc.perform(get("/api/personas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(persona.getId())))
            .andExpect(jsonPath("$.[*].run").value(hasItem(DEFAULT_RUN.toString())))
            .andExpect(jsonPath("$.[*].nombreCompleto").value(hasItem(DEFAULT_NOMBRE_COMPLETO.toString())))
            .andExpect(jsonPath("$.[*].fechaNacimiento").value(hasItem(DEFAULT_FECHA_NACIMIENTO.toString())))
            .andExpect(jsonPath("$.[*].sexo").value(hasItem(DEFAULT_SEXO.toString())))
            .andExpect(jsonPath("$.[*].edadAgnos").value(hasItem(DEFAULT_EDAD_AGNOS)))
            .andExpect(jsonPath("$.[*].edadMeses").value(hasItem(DEFAULT_EDAD_MESES)))
            .andExpect(jsonPath("$.[*].edadDias").value(hasItem(DEFAULT_EDAD_DIAS)))
            .andExpect(jsonPath("$.[*].fechaConsulta").value(hasItem(DEFAULT_FECHA_CONSULTA.toString())));
    }

    @Test
    public void getPersona() throws Exception {
        // Initialize the database
        personaRepository.save(persona);

        // Get the persona
        restPersonaMockMvc.perform(get("/api/personas/{id}", persona.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(persona.getId()))
            .andExpect(jsonPath("$.run").value(DEFAULT_RUN.toString()))
            .andExpect(jsonPath("$.nombreCompleto").value(DEFAULT_NOMBRE_COMPLETO.toString()))
            .andExpect(jsonPath("$.fechaNacimiento").value(DEFAULT_FECHA_NACIMIENTO.toString()))
            .andExpect(jsonPath("$.sexo").value(DEFAULT_SEXO.toString()))
            .andExpect(jsonPath("$.edadAgnos").value(DEFAULT_EDAD_AGNOS))
            .andExpect(jsonPath("$.edadMeses").value(DEFAULT_EDAD_MESES))
            .andExpect(jsonPath("$.edadDias").value(DEFAULT_EDAD_DIAS))
            .andExpect(jsonPath("$.fechaConsulta").value(DEFAULT_FECHA_CONSULTA.toString()));
    }

    @Test
    public void getNonExistingPersona() throws Exception {
        // Get the persona
        restPersonaMockMvc.perform(get("/api/personas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updatePersona() throws Exception {
        // Initialize the database
        personaRepository.save(persona);
        int databaseSizeBeforeUpdate = personaRepository.findAll().size();

        // Update the persona
        Persona updatedPersona = personaRepository.findOne(persona.getId());
        updatedPersona
                .run(UPDATED_RUN)
                .nombreCompleto(UPDATED_NOMBRE_COMPLETO)
                .fechaNacimiento(UPDATED_FECHA_NACIMIENTO)
                .sexo(UPDATED_SEXO)
                .edadAgnos(UPDATED_EDAD_AGNOS)
                .edadMeses(UPDATED_EDAD_MESES)
                .edadDias(UPDATED_EDAD_DIAS)
                .fechaConsulta(UPDATED_FECHA_CONSULTA);

        restPersonaMockMvc.perform(put("/api/personas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPersona)))
            .andExpect(status().isOk());

        // Validate the Persona in the database
        List<Persona> personaList = personaRepository.findAll();
        assertThat(personaList).hasSize(databaseSizeBeforeUpdate);
        Persona testPersona = personaList.get(personaList.size() - 1);
        assertThat(testPersona.getRun()).isEqualTo(UPDATED_RUN);
        assertThat(testPersona.getNombreCompleto()).isEqualTo(UPDATED_NOMBRE_COMPLETO);
        assertThat(testPersona.getFechaNacimiento()).isEqualTo(UPDATED_FECHA_NACIMIENTO);
        assertThat(testPersona.getSexo()).isEqualTo(UPDATED_SEXO);
        assertThat(testPersona.getEdadAgnos()).isEqualTo(UPDATED_EDAD_AGNOS);
        assertThat(testPersona.getEdadMeses()).isEqualTo(UPDATED_EDAD_MESES);
        assertThat(testPersona.getEdadDias()).isEqualTo(UPDATED_EDAD_DIAS);
        assertThat(testPersona.getFechaConsulta()).isEqualTo(UPDATED_FECHA_CONSULTA);
    }

    @Test
    public void updateNonExistingPersona() throws Exception {
        int databaseSizeBeforeUpdate = personaRepository.findAll().size();

        // Create the Persona

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPersonaMockMvc.perform(put("/api/personas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(persona)))
            .andExpect(status().isCreated());

        // Validate the Persona in the database
        List<Persona> personaList = personaRepository.findAll();
        assertThat(personaList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    public void deletePersona() throws Exception {
        // Initialize the database
        personaRepository.save(persona);
        int databaseSizeBeforeDelete = personaRepository.findAll().size();

        // Get the persona
        restPersonaMockMvc.perform(delete("/api/personas/{id}", persona.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Persona> personaList = personaRepository.findAll();
        assertThat(personaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Persona.class);
    }
}
