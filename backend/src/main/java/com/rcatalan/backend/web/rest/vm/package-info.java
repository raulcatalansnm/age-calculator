/**
 * View Models used by Spring MVC REST controllers.
 */
package com.rcatalan.backend.web.rest.vm;
