(function() {
    'use strict';

    angular
        .module('backendApp')
        .controller('PersonaDialogController', PersonaDialogController);

    PersonaDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Persona'];

    function PersonaDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Persona) {
        var vm = this;
        $scope.dateformat = 'dd-MM-yyyy';

        vm.persona = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.persona.id !== null) {
                Persona.update(vm.persona, onSaveSuccess, onSaveError);
            } else {
                Persona.save(vm.persona, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('backendApp:personaUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.fechaNacimiento = false;
        vm.datePickerOpenStatus.fechaConsulta = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
        
        function dateFormat (date) {
        	var dd = date.getDate();
            var mm = date.getMonth() + 1;
            var yyyy = date.getFullYear();
            
            dd = dd > 9 ? dd : '0' + dd;
            mm = mm > 9 ? mm : '0' + mm;
            
            return yyyy + '-' + mm + '-' + dd;
        }
        
        $scope.change = function() {
        	var p = $scope.vm.persona;
        	
        	if(!p.fechaNacimiento)
        		return;
        	
        	var today = new Date();
        
			$.ajax({
				type : "post",
				dataType: 'json',
				url : "age",
				data : JSON.stringify({
					birthday : dateFormat(p.fechaNacimiento), 
					today : dateFormat(today)
				}),
				contentType: 'application/json',
				success: function( response ) {
			        setTimeout(function () {
			            $scope.$apply(function () {
			            	$scope.vm.persona.edadAgnos = response.years;
			            	$scope.vm.persona.edadMeses = response.months;
			            	$scope.vm.persona.edadDias = response.days;
			            	$scope.vm.persona.fechaConsulta = today;
			            });
			        }, 1500);
			    }
			});
			
        };
    }
})();
