package agecalculator;

public class Age {
	private int years;
	private int months;
	private int days;
	
	/**
	 * @return the years
	 */
	public int getYears() {
		return years;
	}
	/**
	 * @param years the years to set
	 */
	public void setYears(int years) {
		this.years = years;
	}
	/**
	 * @return the months
	 */
	public int getMonths() {
		return months;
	}
	/**
	 * @param months the months to set
	 */
	public void setMonths(int months) {
		this.months = months;
	}
	/**
	 * @return the days
	 */
	public int getDays() {
		return days;
	}
	/**
	 * @param days the days to set
	 */
	public void setDays(int days) {
		this.days = days;
	}
	
	@Override
	public String toString() {
		String ageSting = this.years == 1 ? this.years + " year, " : this.years + " years, ";
		ageSting += this.months == 1 ? this.months + " month, " : this.months + " months, ";
		ageSting += this.days == 1 ? this.days + " day, " : this.days + " days.";
		return ageSting;
	}

}
