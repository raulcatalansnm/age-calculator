package com.rcatalan.backend.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Persona.
 */

@Document(collection = "persona")
public class Persona implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @NotNull
    @Pattern(regexp = "^([0-9]+-[0-9K])$")
    @Field("run")
    private String run;

    @Field("nombre_completo")
    private String nombreCompleto;

    @Field("fecha_nacimiento")
    private LocalDate fechaNacimiento;

    @Pattern(regexp = "^(M|F)$")
    @Field("sexo")
    private String sexo;

    @Field("edad_agnos")
    private Integer edadAgnos;

    @Field("edad_meses")
    private Integer edadMeses;

    @Field("edad_dias")
    private Integer edadDias;

    @Field("fecha_consulta")
    private LocalDate fechaConsulta;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRun() {
        return run;
    }

    public Persona run(String run) {
        this.run = run;
        return this;
    }

    public void setRun(String run) {
        this.run = run;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public Persona nombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
        return this;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    public Persona fechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
        return this;
    }

    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getSexo() {
        return sexo;
    }

    public Persona sexo(String sexo) {
        this.sexo = sexo;
        return this;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public Integer getEdadAgnos() {
        return edadAgnos;
    }

    public Persona edadAgnos(Integer edadAgnos) {
        this.edadAgnos = edadAgnos;
        return this;
    }

    public void setEdadAgnos(Integer edadAgnos) {
        this.edadAgnos = edadAgnos;
    }

    public Integer getEdadMeses() {
        return edadMeses;
    }

    public Persona edadMeses(Integer edadMeses) {
        this.edadMeses = edadMeses;
        return this;
    }

    public void setEdadMeses(Integer edadMeses) {
        this.edadMeses = edadMeses;
    }

    public Integer getEdadDias() {
        return edadDias;
    }

    public Persona edadDias(Integer edadDias) {
        this.edadDias = edadDias;
        return this;
    }

    public void setEdadDias(Integer edadDias) {
        this.edadDias = edadDias;
    }

    public LocalDate getFechaConsulta() {
        return fechaConsulta;
    }

    public Persona fechaConsulta(LocalDate fechaConsulta) {
        this.fechaConsulta = fechaConsulta;
        return this;
    }

    public void setFechaConsulta(LocalDate fechaConsulta) {
        this.fechaConsulta = fechaConsulta;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Persona persona = (Persona) o;
        if (persona.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, persona.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Persona{" +
            "id=" + id +
            ", run='" + run + "'" +
            ", nombreCompleto='" + nombreCompleto + "'" +
            ", fechaNacimiento='" + fechaNacimiento + "'" +
            ", sexo='" + sexo + "'" +
            ", edadAgnos='" + edadAgnos + "'" +
            ", edadMeses='" + edadMeses + "'" +
            ", edadDias='" + edadDias + "'" +
            ", fechaConsulta='" + fechaConsulta + "'" +
            '}';
    }
}
