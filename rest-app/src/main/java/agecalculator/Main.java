package agecalculator;
import static spark.Spark.*;

import java.time.LocalDate;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import agecalculator.AgeCalculator;

import static agecalculator.JsonUtil.*;

public class Main {
    /**
     * @param args
     */
    public static void main(String[] args) {
    	
    	port(9090);
    	
    	/**
         * POST que retorna la edad de una persona en Formato JSON
         * conteniedo los años, meses y días.
         * Ej.: { years : 35, months : 2, days : 29 }
         * Los parámetros de entrada se expresan en JSON
         * y especifican la fecha de nacimiento y la fecha
         * actual. Ej.: { birthday : "1980-12-14", today : "2017-01-23" }
         */
    	post("/age", "application/json", (req, res) -> {
        	String json = req.body();
        	if(json != null) {
        		JsonParser jsonParser = new JsonParser();
        		JsonObject jsonObject = (JsonObject) jsonParser.parse(json);
        		String birthdaySt = jsonObject.get("birthday") == null ? null : jsonObject.get("birthday").getAsString();
        		String todaySt = jsonObject.get("today") == null ? null : jsonObject.get("today").getAsString();
        		LocalDate birthday = birthdaySt == null ? null : LocalDate.parse(birthdaySt);
        		LocalDate today = todaySt == null ? null : LocalDate.parse(todaySt);
        		if (birthday != null && today != null) {
        			res.type("application/json");
        			return (new AgeCalculator(birthday)).calculateAge(today);
        		}
        	}
        	return "";
        	
        }, json());
        
    }
    
}